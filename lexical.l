%{
#include <stdlib.h>
#include "parser.tab.h"
%}

%%

[0-9]+        { yylval.number = atoi(yytext); return NUMBER; }
[-+()*/]      { return yytext[0]; }
\n            { return 0; }
[ \t\f\v]     { ; }
.             { ; }

%%

int yywrap(){
    return -1;
}
