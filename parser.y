%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int add_to_table(int, int, char);
void yyerror(char*);
int yylex();

int counter = 0;
int result_number = 0;
int number_of_numbers = 0;
int only_number;

struct expression {
    int operand_one;
    int operand_two;
    char operator;
    int result;
};
%}

%union{
    int number;
}

%left '+' '-'
%left '/' '*'

%token <number> NUMBER
%type <number> E

%%

S : E
  ;

E : E '+' E     { $$ = add_to_table((int) $1, (int) $3, '+'); }
  | E '-' E     { $$ = add_to_table((int) $1, (int) $3, '-'); }
  | E '/' E     { $$ = add_to_table((int) $1, (int) $3, '/'); }
  | E '*' E     { $$ = add_to_table((int) $1, (int) $3, '*'); }
  | '(' E ')'   { $$ = (int) $2; }
  | NUMBER      { $$ = (int) $1; number_of_numbers++; only_number = (int) $1; }
  ;

%%

struct expression list[100]; 

void yyerror(char* message) {
    fprintf(stderr, "%s\n", message);
    exit(1);
}

int add_to_table(int operand_one, int operand_two, char operator) {
    result_number--;
    list[counter].operand_one = operand_one;
    list[counter].operand_two = operand_two;
    list[counter].operator = operator;
    list[counter].result = result_number;
    counter++;
    return result_number;
}

char* convert_number_to_alphabet(char number) {
    if (number == 48) return "Zer";
    if (number == 49) return "One";
    if (number == 50) return "Two";
    if (number == 51) return "Thr";
    if (number == 52) return "Fou";
    if (number == 53) return "Fiv";
    if (number == 54) return "Six";
    if (number == 55) return "Sev";
    if (number == 56) return "Eig";
    if (number == 57) return "Nin";
}

char* calculate_power(int number) {
    if (number == 0) return "";
    if (number == 1) return "Ten_";
    if (number == 2) return "Hun_";
}

void print_operation(char operation) {
    if (operation == 43) printf(" Plu ");
    if (operation == 45) printf(" Min ");
    if (operation == 42) printf(" Mul ");
    if (operation == 47) printf(" Div ");
}

void print_value(int this_value) {
    char value[10];
    itoa(this_value, value, 10);
    int length = strlen(value) - 1;
    char flag = 1;
    for (int counter = 0; counter < strlen(value); counter++) {
        if (length >= 3 && flag == 1) {
	    printf("(");
	    flag = 0;
        }
        printf("%s", convert_number_to_alphabet(value[counter]));
        printf("%s", calculate_power(length % 3));
        if (length == 3 && flag == 0) {
	    printf(")Tou_");
	    flag == 1;
        }
        length--;
    }
}

void three_address_code() {
    int index_counter = 0;
    while (index_counter < counter) {
        printf("assign ");
	if (list[index_counter].operand_one < 0) {
	    printf("t%d", list[index_counter].operand_one * -1);
        } else {
	    print_value(list[index_counter].operand_one);
	}
        print_operation(list[index_counter].operator);
        if (list[index_counter].operand_two < 0) {
	    printf("t%d", list[index_counter].operand_two * -1);
	} else { 
	    print_value(list[index_counter].operand_two);
	}
	printf(" to t%d\n", list[index_counter].result * -1);
        index_counter++;
    }
    if (number_of_numbers == 1) {
        printf("assign ");
        print_value(only_number);
	printf(" to t1\nprint t1\n");
    } else {
        index_counter--;
        printf("print t%d\n", list[index_counter].result * -1);
    }
}

int main() {
    printf("Enter The Expression: ");
    yyparse();
    printf("\nIntermediate Code Is:\n");
    three_address_code();
    return 0;
}
